<?php

/**
 * @file
 * Newsletter popup theme implementation.
 *
 * This template is used to render a fully working newsletter popup.
 *
 * Available variables:
 *   - $title: Message to invite the user to submit an email address.
 *   - $signup_form: The form ready to be rendered (text field and submit).
 *   - $thankyou: Message to confirm the user the email has been saved.
 *   - $description: Brief description of how the data will be used.
 */
?>


<div id="newsletter-prompt" class="<?php print $position; ?> newsletter-hidden">
  <div class="prompt-container">
    <h2 class="prompt-title"><?php print $title; ?></h2>
    <span class="prompt-close"></span>
    <div class="prompt-form">
      <?php print render($signup_form); ?>
    </div>
    <h2 class="prompt-thankyou newsletter-hidden"><?php print $thankyou; ?></h2>
    <h3 class="prompt-description newsletter-hidden"><?php print $description; ?></h3>
  </div>
</div>
