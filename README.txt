
CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration

INTRODUCTION
------------

The Newsletter Prompt module provides a simple but yet comprehensive solution
for managing a popup that collects email addresses, to be used for online
marketing.
The data collected can be (optionally) stored in an internal table or made
available to external modules that implement hook_newsletter_prompt_store_alter.

	* For a full description of the module, visit the project page:
    https://www.drupal.org/sandbox/paolon/2596687

 	* To submit bug reports and feature suggestions, or to track changes:
    https://www.drupal.org/project/issues/2596687


REQUIREMENTS
------------
This module does not require any additional module.


RECOMMENDED MODULES
-------------------
 	* i18n Variable (https://www.drupal.org/project/i18n):
   	Internationalization (i18n) package. Multilingual variables API.

  * Any custom implementation that will make usage of
  	hook_newsletter_prompt_store_alter() to treat the email submitted.
  	The hook will receive a sanitised email address.


INSTALLATION
------------

 	* Install as you would normally install a contributed Drupal module. See:
  	https://drupal.org/documentation/install/modules-themes/modules-7
  	for further information.


CONFIGURATION
-------------

	* If i18n_variable is enabled, every option will be fully localised. The
		presentation of the popup, as well as the data (potentially) stored,
		will be customised by each available locales.

 	* Customise the pop-up look in Configuration » People » Newsletter Prompt:

 		- Select one of the 6 available positions (top, right, bottom-right,
 			bottom, bottom-left, left).

 		- Select the TimeToLive of the "popup closed" cookie (when the user closes
 			the popup without submitting).

 		- Fill in all the explaining text presented in the front-end.

	* Configure the data storage and where to show the popup in Configuration »
			People » Newsletter Prompt » Settings:

		- Enable the internal DB storage if you want to store email addresses.
		!!! NOTE !!! It's not advisable to store any sensible user data, unless
		you have twe explicit consense of the user and you are aware of the local
		legislations.

		- Enable the data propagation, if any hook_newsletter_prompt_store_alter()
			has been implemented by separate code.

		- Select the theme that will present the popup.
