/**
 * @file
 * Setting cookie and displaying a 'thank you' message.
 */

(function ($, Drupal) {
  'use strict';
  // Set the cookie so we don't show the popup anymore (for 10 years).
  $.cookie('newsletter_prompt', 1, {
    expires: 3650,
    path: '/'
  });
  // Hides the form and shows the Thank You message.
  $('#newsletter-prompt .prompt-title, #newsletter-prompt form').hide();
  $('#newsletter-prompt .prompt-thankyou, #newsletter-prompt .prompt-description').fadeIn(800);
  setTimeout(function () {
    $('#newsletter-prompt').fadeOut(500, function () {
      $(this).remove();
    });
  }, 3000);
})(jQuery, Drupal);
