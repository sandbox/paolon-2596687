/**
 * @file
 * Main JS for handling the prompt.
 */

(function ($, Drupal) {
  'use strict';
  $(function () {
    var position = Drupal.settings.newsletter_prompt.position || 'bottom-right';
    $('#newsletter-prompt').addClass(position);
    $('#newsletter-prompt input').attr('placeholder', Drupal.t('Enter your email'));
    var $cookie = $.cookie('newsletter_prompt');
    var scrollingOffset = Drupal.settings.newsletter_prompt.scroll || 500;
    $(window).scroll(function () {
      if ($(window).scrollTop() > scrollingOffset && $cookie === null && $('#sliding-popup:visible').length === 0) {
        $('#newsletter-prompt').fadeIn(800).removeClass('newsletter-hidden');
        var ttl = Drupal.settings.newsletter_prompt.ttl || 365;
        if (typeof ttl === 'string') {
          ttl = new Date(new Date().getTime() + ttl * 86400000);
        }
        $('#newsletter-prompt .prompt-close').click(function () {
          // If the user closes the popup, set the cookie to the TTL provided.
          $.cookie('newsletter_prompt', 1, {expires: ttl});
          $('#newsletter-prompt').fadeOut(800, function () {
            $(this).addClass('newsletter-hidden');
          });
        });
      }
    });
  });
})(jQuery, Drupal);
