<?php
/**
 * @file
 * Admin form for Newsletter Prompt.
 */

/**
 * Presentation settings for Newsletter Prompt.
 */
function newsletter_prompt_ui_admin_form($form, $form_state) {

  // Adds Position options for the popup.
  $positions = array(
    'top' => t('Top'),
    'right' => t('Right'),
    'bottom-right' => t('Bottom Right'),
    'bottom' => t('Bottom'),
    'bottom-left' => t('Bottom Left'),
    'left' => t('Left'),
  );
  $form['newsletter_prompt_position'] = array(
    '#type' => 'select',
    '#title' => t('Position'),
    '#options' => $positions,
    '#default_value' => variable_get('newsletter_prompt_position', 'bottom-right'),
  );

  // The scrolling distance from the top that will trigger the popup.
  $form['newsletter_prompt_scroll'] = array(
    '#type' => 'textfield',
    '#title' => t('The scrolling distance from the top that will trigger the popup (in pixels)'),
    '#default_value' => variable_get('newsletter_prompt_scroll', 500),
    '#size' => 5,
  );

  // Adds TTL options for the cookie set when user closes the popup.
  // The keys (and therefore the values returned by the select element) are the
  // days passed to the JS in the expire property.
  $ttl = array(
    1 => t('Daily'),
    7 => t('Weekly'),
    30 => t('Monthly'),
    365 => t('Yearly'),
  );
  $form['newsletter_prompt_ttl'] = array(
    '#type' => 'select',
    '#title' => t('When to expire the cookie and show the widget again, if the user has closed the popup.'),
    '#options' => $ttl,
    '#default_value' => variable_get('newsletter_prompt_ttl', 0),
  );

  $form['newsletter_prompt_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Text to present to the user when asking for the email address.'),
    '#default_value' => variable_get('newsletter_prompt_title', ''),
  );

  $form['newsletter_prompt_thankyou'] = array(
    '#type' => 'textfield',
    '#title' => t('Thank you message used when the email has been submitted.'),
    '#default_value' => variable_get('newsletter_prompt_thankyou', ''),
  );

  $form['newsletter_prompt_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Describe how the email address will be used.'),
    '#default_value' => variable_get('newsletter_prompt_description', ''),
  );

  return system_settings_form($form);
}

/**
 * Behavioral settings for Newsletter Prompt.
 */
function newsletter_prompt_config_admin_form($form, $form_state) {
  $storage = variable_get('newsletter_prompt_storage', array());
  $storage_drupal_temporary = isset($storage['drupal_temp']) ? $storage['drupal_temp'] : 0;
  $storage_drupal_permanent = isset($storage['drupal']) ? $storage['drupal'] : 0;

  $form["newsletter_prompt_storage"]["#tree"] = TRUE;
  $form['newsletter_prompt_storage']['drupal_temp'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Drupal DB as a temporary buffer.'),
    '#description' => t('!!! CAUTION !!! Due to regulations over privacy and data collection in the country where the DB or the webserver(s) are physically located, this option might be not suitable for you.'),
    '#default_value' => $storage_drupal_temporary,
  );

  $form['newsletter_prompt_storage']['drupal'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Drupal DB as the storage for emails.'),
    '#description' => t('!!! CAUTION !!! Due to regulations over privacy and data collection in the country where the DB or the webserver(s) are physically located, this option might be not suitable for you.'),
    '#default_value' => $storage_drupal_permanent,
  );

  // Retrieve a list of implemented hooks. since this module is supposed to be
  // as agnostic as possible, it will only be allowed to save the data in the
  // Drupal DB. For any other service, just implement
  // hook_newsletter_prompt_store_alter().
  $implemented_hooks = module_implements('newsletter_prompt_store_alter');
  if ($implemented_hooks) {
    $form['newsletter_prompt_storage']['external'] = array(
      '#type' => 'checkbox',
      '#title' => t('Send the emails collected to external providers.'),
      '#default_value' => isset($storage['external']) ? $storage['external'] : 0,
    );

    $form['newsletter_prompt_hooks'] = array(
      '#type' => 'fieldset',
      '#title' => t('The following providers will be used.'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );

    foreach ($implemented_hooks as $provider) {
      $form['newsletter_prompt_hooks'][$provider] = array(
        '#markup' => '<div>' . $provider . '</div>',
      );
    }
  }

  // Options to show the popup per theme.
  $available_themes = array();
  foreach (list_themes() as $theme) {
    if ($theme->status == 1) {
      $available_themes[$theme->name] = $theme->name;
    }
  }
  $form['newsletter_prompt_themes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('What themes will show the popup.'),
    '#options' => $available_themes,
    '#default_value' => variable_get('newsletter_prompt_themes', array()),
  );
  return system_settings_form($form);
}

/**
 * Implements hook_form_validate().
 */
function newsletter_prompt_ui_admin_form_validate($form, $form_state) {
  // Making sure the scrolling offset passed is a number.
  $form_state['values']['newsletter_prompt_scroll'] = (int) $form_state['values']['newsletter_prompt_scroll'];
}
